COMPILER_FLAGS = -Wall -g -O3 -I/usr/local/include/opencv4
LINKER_FLAGS = -lopencv_calib3d -lopencv_core -lopencv_dnn -lopencv_features2d -lopencv_flann -lopencv_gapi -lopencv_highgui -lopencv_imgcodecs -lopencv_imgproc -lopencv_ml -lopencv_objdetect -lopencv_photo -lopencv_stitching -lopencv_video -lopencv_videoio

OBJ_NAME = main

all:
	$(CXX) main.cpp $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)
